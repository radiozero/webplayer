# RadioZero Web Player
- O player possui as seguintes dependências: jQuery(http://jquery.com) e MediaElement(http://mediaelementjs.com).
- A versão do jQuery distribuída neste repositório é a 1.11.3.
- A versão do MediaElement distribuída neste repositório é a 2.18.1.
- O player está implementado no arquivo player.js.

### play() - ativação do player
- O primeiro argumento indica a origem para execução. Deve sempre ser 'http://radiozero.com.br', ou 'https://radiozero.com.br', de acordo com o protocolo a ser utilizado.
- O segundo argumento indica o nome da rádio a ser executada. É o mesmo nome da estação que está disponível em http://radiozero.com.br/estacao/
- O terceiro argumento é um seletor CSS que indica o elemento a HTML a ser tratado como player.
- O quarto argumento é um seletor CSS que indica o elemento a HTML a ser tratado como botão para iniciar a execução. 'false' indica que nenhum botão será utilizado.
> Caso necessário, algumas informações para depuração são jogadas ao log do navegador.

### Exemplo de utilização - Carrega a rádio 'RadioZero' utilizando HTTPS
```
<script>
    $(document).ready(function(){
        play('https://radiozero.com.br', 'RadioZero', '#player', false);
    });
</script>
```

### Detalhes de implementação
Neste repositório, o MediaElement está disponível através do diretório 'mediaelement'. Além de oferecer suporte para HTML5, o MediaElement também oferece modos de compatibilidade utilizando Silverlight e Flash, que são ativados automaticamente quando o suporte para HTML5 não é detectado. Para tal, o caminho contendo os plugins de compatibilidade deve ser informado ao MediaElement.
No arquivo player.js, podemos verificar essa informação na linha 36.
```
    $(selector).mediaelementplayer({
        audioWidth: 300,
        audioHeight: 30,
        features: ['playpause', 'progress', 'duration'],
        pluginPath: '/mediaelement/',
        plugins: ['flash', 'silverlight'],
        flashName: 'flashmediaelement.swf',
        silverlightName: 'silverlightmediaelement.xap',
        success: function(mediaElement, domObject){
            console.log('Successful call to loadMediaElement()')
            result = true;
            if (typeof(callback) !== 'undefined') {
                callback(mediaElement, domObject);
            }
        },
        error: function(){
            console.log('Error when calling loadMediaElement()');
            result = false;
        }
    });
```
Caso seja desejado distribuir o MediaElement em outro diretório, a linha 36 do arquivo player.js deve ser editada, e o caminho apropriado deve ser especificado.

### Versão
0.1