function makeBaseURL(base_url){
    if (base_url.slice(-1) != '/') {
        base_url = base_url + '/';
    }
    return base_url;
}

function loadPlaylist(url, callback){
    var result = false;
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: url,
        async: false,
        success: function(playlist){
            console.log('Successful call to loadPlaylist()')
            result = true;
            if ((playlist.length > 0) && (typeof(callback) !== 'undefined')) {
                callback(playlist);
            }
        },
        error: function(){
            console.log('Error when calling loadPlaylist()');
            result = false;
        }
    });
    return result;
}

function loadMediaElement(selector, callback){
    var result = false;
    $(selector).mediaelementplayer({
        audioWidth: 300,
        audioHeight: 30,
        features: ['playpause', 'progress', 'duration'],
        pluginPath: '/mediaelement/',
        plugins: ['flash', 'silverlight'],
        flashName: 'flashmediaelement.swf',
        silverlightName: 'silverlightmediaelement.xap',
        success: function(mediaElement, domObject){
            console.log('Successful call to loadMediaElement()')
            result = true;
            if (typeof(callback) !== 'undefined') {
                callback(mediaElement, domObject);
            }
        },
        error: function(){
            console.log('Error when calling loadMediaElement()');
            result = false;
        }
    });
    return result;
}

function canLoadTrack(url){
    var result = false;
    $.ajax({
        type: 'HEAD',
        url: url,
        async: false,
        complete: function(jqxhr, textStatus){
            if (textStatus == 'success' || textStatus == 'nocontent') {
                result = true;
            }
        }
    });
    return result;
}

function onTrackEnded(base_url, radio, playlist, mediaElement){
    var track_counter = 0;
    console.log(mediaElement);
    base_url = makeBaseURL(base_url);

    function handler(event){
        var next_track = true;
        while(next_track){
            console.log('Incrementing playlist counter');
            console.log('Previous value: '+track_counter);
            track_counter++;
            console.log('Current value: '+track_counter);
            console.log('Current track: '+playlist[track_counter]);

            if(typeof(playlist[track_counter]) === 'undefined'){
                console.log('Undefined track found. Attempting to fetch new playlist.');
                loadPlaylist(base_url+'get_playlist/'+radio, function(new_playlist){
                    playlist = new_playlist;
                    track_counter = 0;
                });
            }

            if (playlist) {
                track_url = base_url+'get_track/'+playlist[track_counter]+'/'+radio;
                console.log('Attempting to play song from this URL: '+track_url);
                if (canLoadTrack(track_url)) {
                    console.log("Track is loadable.")
                    mediaElement.setSrc(track_url);
                    mediaElement.play();
                    next_track = false;
                }
            }
        }
    };
    return handler;
}

function play(base_url, radio, player_selector, button_selector){
    base_url = makeBaseURL(base_url);
    loadPlaylist(base_url+'get_playlist/'+radio, function(playlist){
        var track_counter = 0;
        console.debug(playlist);
        console.log('Track counter: '+track_counter);

        loadMediaElement(player_selector, function(mediaElement, domObject){
            console.log('MediaElementPlayer applied.');

            var track_url = base_url+'get_track/'+playlist[track_counter]+'/'+radio;
            console.log('Playing song from this URL: '+track_url);
            mediaElement.setSrc(track_url);
            mediaElement.setCurrentTime = function(){};

            if (button_selector) {
                $(button_selector).click(function(){
                    mediaElement.play();
                });
            }

            mediaElement.addEventListener('ended', onTrackEnded(base_url, radio, playlist, mediaElement));
        });
    });
}
